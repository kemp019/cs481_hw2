﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Calculator
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        double num1;
        double num2;
        string op;
        bool doneOp;

        public MainPage()
        {
            InitializeComponent();
            calcClear();
        }

        private void addNum(double n)
        {
            if (!doneOp)
                num1 = (num1 * 10) + n;
            else
                num2 = (num2 * 10) + n;
        }
        private void updateOutput()
        {
            if (!doneOp)
                OutputLine.Text = num1.ToString();
            else
                OutputLine.Text = num1.ToString() + " " + op + " " + num2.ToString();
        }
        private string doOperation()
        {
            double soln = 0;
            switch(op)
            {
                case "/":
                    if (num2 != 0)
                        soln = num1 / num2;
                    else
                        return "Undefined";
                    break;
                case "x":
                    soln = num1 * num2;
                    break;
                case "+":
                    soln = num1 + num2;
                    break;
                case "-":
                    soln = num1 - num2;
                    break;
                case "":
                    soln = num1;
                    break;
                default:
                    break;
            }
            num1 = soln;
            return soln.ToString();
        }
        private void calcClear()
        {
            OutputLine.Text = "Please Enter a number to begin";
            num1 = 0;
            num2 = 0;
            op = "";
            doneOp = false;
        }
        
        private void flipSign()
        {
            if (!doneOp)
                num1 *= -1;
            else
                num2 *= -1;
        }
        private void OnClearClicked(object sender, EventArgs args)
        {
            calcClear();
        }
        private void OnFlipClicked(object sender, EventArgs args)
        {
            flipSign();
            updateOutput();
        }

        private void OnNumberClicked(object sender, EventArgs args)
        {
            Button numbut = (Button)sender;
            double butnum;
            double.TryParse(numbut.Text, out butnum);
            addNum(butnum);
            updateOutput();
        }
        private void OnOperationClicked(object sender, EventArgs args)
        {
            Button opbut = (Button)sender;
            op = opbut.Text;
            doneOp = true;
            updateOutput();
        }
        private void OnEqualClicked(object sender, EventArgs args)
        {
            OutputLine.Text = doOperation();
            num2 = 0;
            op = "";
            doneOp = false;
        }
    }
}
